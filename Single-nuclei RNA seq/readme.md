The input files for the R sript were produced following the steps described in the Methods section:

The reads were mapped to the Drosophila reference genome (dm6) plus the eGFP-Bicoid plasmid sequence and counted with Cell Ranger (6.0.1), with intronic reads included. 

Versions of packages:
		
R	4.2.2-foss-2022b
- seurat	3.9.9.9010		
- harmony	0.1.1
- dplyr	1.1.0
- ggplot2	3.4.1
- ggalluvial	0.12.5
- stringr	1.5.0
- cowplot	1.1.1
- reshape2	1.4.4

The Seurat object "evol.big" can be provided upon request.