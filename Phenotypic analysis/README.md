- The R codes were developed under R version 4.3.2.

- package versions:
	
	- ggplot2	3.4.4
	- reshape2	1.4.4
	- dplyr	1.1.4
	- stringr	1.5.1
	- scales	1.3.0
	- ggnewscale	0.4.9
	- nlme	3.1-163
	- emmeans	1.10.0
	- cowplot	1.1.3
	- FactoMineR	2.11
	- factoextra	1.0.7
	- corrplot	0.92

- The codes don't require installation.

- Source input data are provided under "data".
- The R commands in "script" should run in R or RStudio directly (no instructions are required).
- Expected output are provided as figures or as supplementary data files in the manuscript.
